# Piveau-UI

Repository built for integrating Piveau Frontend apps and
components in the form of a monorepo. 

## Quickstart

### Prerequisites

- [Node.js](https://nodejs.org/en/) (version 18 or higher)

### Running the demo app

```
cp apps/modules-demo/config/user-config.sample.js apps/modules-demo/config/user-config.js
npm ci
npm run dev
```

## Contributing

Please see [CONTRIBUTING.md](CONTRIBUTING.md) for details.