const config = {
  datasets: {
    step1: {
      title: {},
      datasetID: {},
      description: {},
      catalog: {},
      publisher: {},
      theme: {},
      issued: {},
      modified: {},
    },
    step2: {
      keyword: {},
      subject: {},
      contactPoint: {},
      landingPage: {},
      accrualPeriodicity: {},
      language: {},
      spatial: {},
      temporal: {},
      creator: {},
      identifier: {},
      admsIdentifier: {},
      page: {},
      accessRights: {},
    },
    step3: {
      type: {},
      isUsedBy: {},
      conformsTo: {},
      versionInfo: {},
      versionNotes: {},
      temporalResolution: {},
      spatialResolutionInMeters: {},
      relation: {},
      qualifiedRelation: {},
      isReferencedBy: {},
      hasVersion: {},
      isVersionOf: {},
      source: {},
      provenance: {},
      qualifiedAttribution: {},
      wasGeneratedBy: {},
    }
  },
  distributions: {
    step1: {
      accessURL: {},
      title: {},
      description: {},
    },
    step2: {
      downloadUrl: {},
      format: {},
      mediaType: {},
      licence: {},
      status: {},
      availability: {},
      issued: {},
      modified: {},
    },
    step3: {
      type: {},
      byteSize: {},
      checksum: {},
      compressFormat: {},
      packageFormat: {},
      language: {},
      page: {},
      conformsTo: {},
      rights: {},
      hasPolicy: {},
      temporalResolution: {},
      spatialResolutionInMeters: {},
    },
    step4: {
      accessService: {},
    }
  },
  catalogues: {
    step1: {
      title: {},
      datasetID: {},
      description: {},
      publisher: {},
      language: {},
      homepage: {},
      licence: {},
    },
    step2: {
      spatial: {},
      hasPart: {},
      isPartOf: {},
      // rights: {},
      catalog: {},
      creator: {}, 
    }
  }
};

export default config;
