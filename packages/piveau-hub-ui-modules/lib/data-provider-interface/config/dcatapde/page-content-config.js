const config = {
    datasets: {
      step1: {
        title: {},
        datasetID: {},
        description: {},
        catalog: {},
        publisher: {},
        theme: {},
        issued: {},
        modified: {},
  
      },
      step2: {
        politicalGeocodingLevelURI: {},
        politicalGeocodingURI: {},
        availabilityDE: {},
        contributorID: {},
        geocodingDescription: {},
        legalBasis: {},
        qualityProcessURI: {},
        references: {},
        contributor: {},
        originator: {},
        maintainer: {},
        keyword: {},
        subject: {},
        contactPoint: {},
        landingPage: {},
        accrualPeriodicity: {},
        language: {},
        spatial: {},
        temporal: {},
        creator: {},
        identifier: {},
        admsIdentifier: {},
        page: {},
        accessRights: {},
      },
      step3: {
        type: {},
        isUsedBy: {},
        conformsTo: {},
        versionInfo: {},
        versionNotes: {},
        temporalResolution: {},
        spatialResolutionInMeters: {},
        relation: {},
        qualifiedRelation: {},
        isReferencedBy: {},
        hasVersion: {},
        isVersionOf: {},
        source: {},
        provenance: {},
        qualifiedAttribution: {},
        wasGeneratedBy: {},
      }
    },
    distributions: {
      step1: {
        accessURL: {},
        title: {},
        description: {},
  
      },
      step2: {
        license: {},
        licenseAttributionByText: {},
        downloadUrl: {},
        format: {},
        mediaType: {},
        status: {},
        availability: {},
        issued: {},
        modified: {},
      },
      step3: {
        type: {},
        byteSize: {},
        checksum: {},
        compressFormat: {},
        packageFormat: {},
        language: {},
        page: {},
        conformsTo: {},
        rights: {},
        hasPolicy: {},
        temporalResolution: {},
        spatialResolutionInMeters: {},
      },
      step4: {
        accessService: {},
        availabilityDisDE: {}
      }
    },
    catalogues: {
      step1: {
        title: {},
        availabilityCatDE: {},
        datasetID: {},
        description: {},
        publisher: {},
        language: {},
        homepage: {},
        licence: {},
      },
      step2: {
        spatial: {},
        hasPart: {},
        isPartOf: {},
        // rights: {},
        catalog: {},
        creator: {},
        
      }
    }
  };
  
  export default config;
  