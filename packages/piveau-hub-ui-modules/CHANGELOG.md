## [3.16.33](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.16.32...v3.16.33) (2024-04-17)


### Bug Fixes

* ConditionalInput.vue shows select values ([073e65a](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/073e65a2728a771a98b284ca4682137a97d4a79b))


### Features

* add Piveau-cookie-consent package ([d4595af](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/d4595af2d0ad8156000a15f5a1ac71902e45747f))



## [3.16.32](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.16.31...v3.16.32) (2024-03-27)



## [3.16.31](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.16.30...v3.16.31) (2024-03-27)


### Bug Fixes

* similar datasets links ([bbea9f8](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/bbea9f822a5f9081728f284cd3a8530db07a2d89))



## [3.16.30](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.16.29...v3.16.30) (2024-03-27)


### Bug Fixes

* similar datasets links ([3dfbf76](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/3dfbf76ab3bc0df4f90ae0b94896857fd5c025e6))



## [3.16.29](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.16.28...v3.16.29) (2024-03-20)



## [3.16.28](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.16.27...v3.16.28) (2024-03-19)


### Bug Fixes

* better treatment of similar datasets breakpoints ([984d09d](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/984d09d0f34d3503d82112bc50bb7dbfb4ca5c8b))



## [3.16.27](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.16.26...v3.16.27) (2024-03-14)


### Bug Fixes

* change data preparation for similar datasets ([8ebe2c3](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/8ebe2c329654ee93bb434ff150b85b0804f3fc3b))



## [3.16.26](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.16.25...v3.16.26) (2024-03-14)


### Bug Fixes

* adapt similar datasets component to knn_request requirements (English description!) ([58bf39f](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/58bf39fe3070f840e363d2e00887ba6ea9a663e6))



## [3.16.25](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.16.24...v3.16.25) (2024-03-13)



## [3.16.24](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.16.23...v3.16.24) (2024-03-07)


### Bug Fixes

* avoid duplicate navigation ([5c8d4fd](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/5c8d4fd2f34f58652e68d8ac8858d739231d9843))



## [3.16.23](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.16.22...v3.16.23) (2024-03-06)



## [3.16.22](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.16.21...v3.16.22) (2024-03-01)



## [3.16.21](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.16.20...v3.16.21) (2024-02-29)



## [3.16.20](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.16.19...v3.16.20) (2024-02-27)


### Bug Fixes

* integrated id validity check into mandatory checking ([b2bf9f4](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/b2bf9f45b587acb17d0d54914237e8619b3edaea))



## [3.16.19](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.16.18...v3.16.19) (2024-02-26)



## [3.16.18](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.16.17...v3.16.18) (2024-02-26)


### Bug Fixes

* **Datasets.vue:** set catalog facet if ctlg_id route param is set ([4d18e26](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/4d18e267085ab85e2c034e7d2680d3008c4c98c1))



## [3.16.17](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.16.16...v3.16.17) (2024-02-26)



## [3.16.16](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.16.15...v3.16.16) (2024-02-23)



## [3.16.15](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.16.14...v3.16.15) (2024-02-23)


### Bug Fixes

* old similarity service reactivated ([620b084](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/620b0844f1c90fca3965b731e5769a9926dd97fe))



## [3.16.14](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.16.13...v3.16.14) (2024-02-22)


### Bug Fixes

* adjust fallback value for dataset title 'No title available' -> '' ([8b7b333](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/8b7b33397dc0be57532767ae34ce60b19ccfdbe9))
* **datasets:** un-hardcode fallback dataset description value ([fdfeb81](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/fdfeb81c36d1de7e98d8a63331033c766d00422f))
* distribution description handling ([976760f](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/976760fb13b2ae250fa8e8f92da93d27dca8f000))
* unhardcode distribution title handling ([a96d146](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/a96d146544b726ddd7fc6f353e50e71e5c701a3f))



## [3.16.13](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.16.12...v3.16.13) (2024-02-21)


### Bug Fixes

* **Datasets:** set superCatalog facet if fixedCatalogFilter is set ([b49b698](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/b49b698698f1d92f233e4bc7a5b8541cfe247207))



## [3.16.12](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.16.11...v3.16.12) (2024-02-19)


### Bug Fixes

* fixed check for mandatory properties of dataservice ([1ffa1a7](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/1ffa1a77693920d4437000faf79d497151a81e22))
* fixed duplicated creator caused by publisher conditional field ([c841c43](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/c841c438b59f12e9f95e5d77993388b376afa537))
* fixed handling of converted temporal resolution for seconds data ([b9c5bae](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/b9c5bae12e0be9ad362c374f9cfab6168fb1fd7a))



## [3.16.11](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.16.10...v3.16.11) (2024-02-13)



## [3.16.10](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.16.9...v3.16.10) (2024-02-12)


### Bug Fixes

* SelectedFacetsOverview.vue: special treatment of erpd supercatalog ([3f43c08](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/3f43c085b4b6a62ce501bfbedc1cf7cb07cac6e1))



## [3.16.9](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.16.8...v3.16.9) (2024-02-12)



## [3.16.8](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.16.7...v3.16.8) (2024-02-08)



## [3.16.7](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.16.5...v3.16.7) (2024-02-07)



## [3.16.5](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.16.3...v3.16.5) (2024-02-06)


### Bug Fixes

* adapt to new supercatalog backend (in datasets, supercatalog is a facet now instead of an extra parameter) ([a67bb71](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/a67bb71a6cd370ae1faeb6db94b53c6c0676e502))
* issue 3373 V2 added style="float: left!important;  https://gitlab.fokus.fraunhofer.de/piveau/organisation/piveau-scrum-board/-/issues/3373 ([2a5bcc6](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/2a5bcc69c502f0b4d62a9424f2a28ecd853d071a))



## [3.16.3](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.16.2...v3.16.3) (2024-02-06)


### Bug Fixes

* issue 3373 Remove text-underline from "embed Feature"  https://gitlab.fokus.fraunhofer.de/piveau/organisation/piveau-scrum-board/-/issues/3373 ([b6355da](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/b6355da54f6d31984bc535e0b063f480121f94c9))



## [3.16.2](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.16.1...v3.16.2) (2024-02-05)


### Bug Fixes

* issue 3373 Remove text-underline from "embed Feature"  https://gitlab.fokus.fraunhofer.de/piveau/organisation/piveau-scrum-board/-/issues/3373 ([7ddde09](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/7ddde094a2aaea6a98e61b7727e507f80a0e0787))



## [3.16.1](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.16.0...v3.16.1) (2024-02-05)



# [3.16.0](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.15.17...v3.16.0) (2024-01-26)


### Features

* **dcat:** extend dataset model with additional high-value-dataset properties ([cc0233f](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/cc0233fdf997e79ab501ab32803e8a9c524c433d))



## [3.15.17](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.15.16...v3.15.17) (2024-01-25)


### Bug Fixes

* Added filtering by subject ([3393fa3](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/3393fa3c2250032900d6f7e8bf15ed361ccd1527))



## [3.15.16](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.15.15...v3.15.16) (2024-01-25)


### Bug Fixes

* added env variable to allow hiding preview button ([dc70e39](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/dc70e39f84ddc06a659c5cb760a758ee5bc5d7da))



## [3.15.15](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.15.14...v3.15.15) (2024-01-24)


### Bug Fixes

* **Datasets,DatasetsFacets:** adjust fixedCatalogFilter setting to set superCatalogue query param instead of setting catalog facet ([99a714b](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/99a714b3dded7cf0d52dcba12e160838dc90512e))



## [3.15.14](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.15.13...v3.15.14) (2024-01-19)


### Bug Fixes

* erpd key changed ([81dd4ea](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/81dd4ead243a5dc581c92006951c171e8f981118))



## [3.15.13](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.15.12...v3.15.13) (2024-01-19)


### Bug Fixes

* proper passing of field property ([52c5d95](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/52c5d9543db594591f604b7bf51fd4fc9d1d77d3))



## [3.15.12](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.15.11...v3.15.12) (2024-01-18)


### Bug Fixes

* properties loaded correctly ([84ace2d](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/84ace2d5e9f41281e443952f116a02936321d74b))



## [3.15.11](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.15.10...v3.15.11) (2024-01-18)


### Bug Fixes

* renaming of variables ([f1f950a](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/f1f950af65b1e420e39792f835f73a28b56c54b4))



## [3.15.10](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.15.9...v3.15.10) (2024-01-17)


### Bug Fixes

* erpd_title_translation ([198b2db](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/198b2db253dc0551a1383257253a1570b7b2e3db))
* renaming of variables ([734f31d](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/734f31d7855d19a5d21a201e4087e5c4961ada5b))



## [3.15.9](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.15.8...v3.15.9) (2024-01-16)


### Bug Fixes

* change default creator position in dsd properties ([1f71655](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/1f71655432e176635b2864bd236376aa78e317e0))



## [3.15.8](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.15.7...v3.15.8) (2024-01-03)



## [3.15.7](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.15.6...v3.15.7) (2023-12-22)


### Bug Fixes

* tooltips and documentation ([2e32736](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/2e3273662ee9c7543f5e9719ffd48729167e8b21))



## [3.15.6](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.15.5...v3.15.6) (2023-12-22)


### Bug Fixes

* typescript errors and dcatapde specification name ([42748b4](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/42748b477f665d9b5289238acc56b393b40a23cd))



## [3.15.5](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.15.4...v3.15.5) (2023-12-21)


### Bug Fixes

* correct translation key for geocodingDescriptionDe ([57aea35](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/57aea354100fc3b8e44310a5f6affde181c274ea))



## [3.15.4](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.15.2...v3.15.4) (2023-12-21)


### Bug Fixes

* change position of creator in dcat spec ([a688a28](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/a688a2885252f670a660bf247809ea8be11d615c))



## [3.15.2](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.15.1...v3.15.2) (2023-12-21)


### Bug Fixes

* styles in properties box ([80d2dc1](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/80d2dc19588e223542a8f8a5ad010fb052b72d26))



## [3.15.1](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.15.0...v3.15.1) (2023-12-21)


### Bug Fixes

* correct expanded property in dsd-properties ([f988e53](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/f988e53012d31f3c5c82b4662606155382eea3d3))



# [3.15.0](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.14.12...v3.15.0) (2023-12-21)


### Features

* make dsd properties configurable ([1dd9aa8](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/1dd9aa87abf2e79bfceb597fa40fa887b54ed26f))



## [3.14.12](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.14.11...v3.14.12) (2023-12-13)


### Bug Fixes

* declare default value for similarity endpoint ([c81818b](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/c81818bf61d358a7ff35dd5182fb6e507d927491))
* declare similarityEndpoint property in config ([b6835ee](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/b6835eedbcfb81f3f81eb108122c118049ed003e))
* unsing app-link in similar datasets component ([149e521](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/149e521bd7547df059b5b0eaf8ad7a75a23785b0))
* variable domain link for similar datasets ([33f6764](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/33f6764e92645de880bdd90789337012cb97265a))


### Features

* adapt get new similar dataset service to work with ui networking ([d226163](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/d2261630be2759a1e8417b5fe908cc84b639d4aa))



## [3.14.11](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.14.10...v3.14.11) (2023-12-04)


### Bug Fixes

* fixed catalog tooltips ([5176d05](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/5176d05af2d041c9c2034885ff0ebc12e00c7f94))



## [3.14.10](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.14.9...v3.14.10) (2023-12-01)


### Features

* issue 3210 add key translate message.metadata.protecteddata ([164269b](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/164269bde1909beff2305d8689345dd4af0efc7b))



## [3.14.9](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.14.7...v3.14.9) (2023-11-30)


### Features

* issue 3210 add key translate the label ‘Dataset’ to ‘Protected data’ if it is a ERPD dataset. ([2b18c4c](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/2b18c4c673319de2f558e06979984e9dbceccd3d))



## [3.14.6](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.14.5...v3.14.6) (2023-11-24)


### Bug Fixes

* release ([be95841](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/be9584134326fc6cbb71fe1c475e84aa97021ae6))



## [3.14.5](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.14.4...v3.14.5) (2023-11-24)



## [3.14.4](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.14.3...v3.14.4) (2023-11-14)



## [3.14.7](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.14.6...v3.14.7) (2023-11-27)



## [3.14.6](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.14.5...v3.14.6) (2023-11-24)


### Bug Fixes

* release ([be95841](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/be9584134326fc6cbb71fe1c475e84aa97021ae6))



## [3.14.5](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.14.4...v3.14.5) (2023-11-24)



## [3.14.4](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.14.3...v3.14.4) (2023-11-14)



## [3.14.3](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.14.2...v3.14.3) (2023-11-10)



## [3.14.2](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.14.1...v3.14.2) (2023-11-09)


### Bug Fixes

* quickfix ([f603a2c](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/f603a2c8be2007604ed60a8736d87db76ccd8240))



## [3.14.1](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.14.0...v3.14.1) (2023-11-09)


### Bug Fixes

* fixed content type ([7a0ca4c](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/7a0ca4cd804c28f6e27363e60e9ee443638d6154))
* new prefixes for the catalog ([20a1bae](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/20a1bae555a360f3936318a39f9be13d74d74ea6))



# [3.14.0](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.13.4...v3.14.0) (2023-11-07)


### Features

* **DPIMenu:** add scoped slot in login-logout element block ([92a2abb](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/92a2abbe7aa433568b5768b198b1c3e0e418195e))



## [3.13.4](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.13.3...v3.13.4) (2023-10-31)


### Bug Fixes

* DQV Data HEAD request ([a4ca611](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/a4ca611f1616f213ca64ea4185e95780ae613788))



## [3.13.3](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.13.2...v3.13.3) (2023-10-25)



## [3.13.2](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.13.1...v3.13.2) (2023-10-18)


### Bug Fixes

* **Datasets:** fix fixed catalog facets mode loading all datasets; improve stability of retrieving facets from query params ([b00f333](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/b00f333c52f63e9cdc66c82af3e0b620641748a4))



## [3.13.1](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.13.0...v3.13.1) (2023-10-16)



# [3.13.0](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.12.1...v3.13.0) (2023-10-16)


### Bug Fixes

* new Id Validation Rule ([2e6d4d8](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/2e6d4d8908861c7bfdce3d900f4504b08563701a))


### Features

* **Datasets:** implement fixedCatalogFilter; hide catalog related facets in SelectedFacets and Facets ([376816f](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/376816fb1b4e50e85fcccb8c785bc57e2113c2fe))



## [3.12.1](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.11.7...v3.12.1) (2023-10-10)



# [3.12.0](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.11.6...v3.12.0) (2023-10-09)



## [3.11.7](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.12.0...v3.11.7) (2023-10-10)


### Bug Fixes

* facet selection ([56fafbc](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/56fafbccdc1606db22cf6605feba798096bbcb4b))



# [3.12.0](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.11.6...v3.12.0) (2023-10-09)



## [3.11.6](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.11.5...v3.11.6) (2023-10-06)


### Bug Fixes

* gazetteer store API response structure ([3c5224d](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/3c5224d0fe4bbfcf78f455f7f79954ec4fc734e7))



## [3.11.5](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.11.4...v3.11.5) (2023-10-04)



## [3.11.4](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.11.3...v3.11.4) (2023-10-04)


### Bug Fixes

* fixed overview page ([8254613](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/8254613868c699dbb5a1ce82dd3f01875ea52b67))
* new overview Page ([4d54a4f](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/4d54a4f484a29304e971708d906de4185974fce8))



## [3.11.3](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.11.2...v3.11.3) (2023-10-02)



## [3.11.2](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.11.1...v3.11.2) (2023-09-29)


### Bug Fixes

* fixed name search for vocabularies ([459188f](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/459188f717eafc8008e46b94463a661f2e652754))



## [3.11.1](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.11.0...v3.11.1) (2023-09-29)


### Bug Fixes

* supercatalog metadata request safe ([2db63e3](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/2db63e392fc33e63992774112ee30e230213c0ab))



# [3.11.0](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.10.13...v3.11.0) (2023-09-29)



## [3.10.13](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.10.12...v3.10.13) (2023-09-27)


### Bug Fixes

* fixed DEU Feedback ([099a157](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/099a15789ec44e273ce5113f72db8adc6bfdced4))
* new clear message (Also made the text red) ([de491ba](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/de491ba6f0e0bdd5fbabadedeaff6e00d76e9724))



## [3.10.12](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.10.11...v3.10.12) (2023-09-22)


### Bug Fixes

* **CatalogPage:** use dedicated xhr to search api for fallback interested datasets ([e4eaf94](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/e4eaf94edca8bc270f217907b420b46738ea9d9e))
* **Datasets:** fix catalogs facets filter not being applied when toggling ([24861a0](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/24861a0c8fff614d88d1975e2a2badce1d631ba3))



## [3.10.11](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.10.10...v3.10.11) (2023-09-22)


### Bug Fixes

* fixed url handling for fileuploads within DPI ([0bd6a0b](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/0bd6a0b45296bdf8a95df14067aeafa927117236))



## [3.10.10](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.10.9...v3.10.10) (2023-09-21)


### Bug Fixes

* state ([ee4cc56](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/ee4cc566a2fd445c57c3c2d1a5f6321ae68f7ffd))



## [3.10.9](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.10.8...v3.10.9) (2023-09-21)


### Bug Fixes

* temporary fix, only get catalog name from augsburg domain if not in parameter ([1b5673a](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/1b5673a40284c7ce7ec59ab20b5aa707c5abd8e9))



## [3.10.8](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.10.7...v3.10.8) (2023-09-21)


### Bug Fixes

* add inCatalogPage prop to Datasets.vue ([b7ac019](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/b7ac0195b86214379320c337cec888d79b17e20a))



## [3.10.7](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.10.6...v3.10.7) (2023-09-21)


### Bug Fixes

* add catalog field to computed in datasets ([b297a4f](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/b297a4f652f7802ab365c984768b1532e253890e))
* fix build (one too many curly bracket) ([550842a](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/550842aa99f7287573f744f87e32076634b827ff))



## [3.10.6](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.10.5...v3.10.6) (2023-09-21)


### Bug Fixes

* get catalog id from domain ([01368da](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/01368da6fae3f48313b66897e2a44f0925634ed5))



## [3.10.5](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.10.4...v3.10.5) (2023-09-21)


### Bug Fixes

* remove loggers ([6d9a426](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/6d9a42644f62e3dc290241522bf2bd58d8c5e796))



## [3.10.4](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.10.3...v3.10.4) (2023-09-21)


### Bug Fixes

* trial to fix issue with showing the right datasets for a specific catalog page in an external domain ([664e424](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/664e4245f34e10dd8e9eeeeb8011ef849671c47e))



## [3.10.3](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.10.2...v3.10.3) (2023-09-20)



## [3.10.2](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.10.1...v3.10.2) (2023-09-20)


### Bug Fixes

* minor styling and text change ([5f3b5a2](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/5f3b5a2a2c20bf1a47c6a1ecaa1e1d457daab454))



## [3.10.1](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.10.0...v3.10.1) (2023-09-19)


### Bug Fixes

* fixed Fileupload ([0a10ee6](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/0a10ee68b8ee904ac9abe20571e1da270a312937))



# [3.10.0](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.9.19...v3.10.0) (2023-09-18)


### Bug Fixes

* only allows properly formatted JSONs.  ([e93e60e](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/e93e60e8877bc551f7989ac1cdcfeb43fa154a35))



## [3.9.19](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.9.18...v3.9.19) (2023-09-01)



## [3.9.18](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.9.17...v3.9.18) (2023-08-31)



## [3.9.17](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.9.16...v3.9.17) (2023-08-31)


### Bug Fixes

* added logging to Catalogues ([ed1fdea](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/ed1fdea43214fa96141c9b4e7729c3db251a0e56))
* fixed Catalogue overview ([e220f8c](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/e220f8c0d4bbb850a236dc3678bf97d66162d755))



## [3.9.16](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.9.15...v3.9.16) (2023-08-30)



## [3.9.15](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.9.14...v3.9.15) (2023-08-30)


### Bug Fixes

* formatting ([b37fe6c](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/b37fe6c9e51dd6ef9e1a56bd9efe7018e47dc56e))



## [3.9.14](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.9.13...v3.9.14) (2023-08-30)


### Bug Fixes

* fixed dcatap-de ([364ec72](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/364ec7247682e6e7e47446cf0458a06ffe2b17c3))



## [3.9.13](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.9.12...v3.9.13) (2023-08-30)


### Features

* Evaluate Array values in Runtime Configuration by using JSON.parse ([1f1ba0e](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/1f1ba0ef7e15b7f817b0fed53fd45c129cc723dc))



## [3.9.12](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.9.11...v3.9.12) (2023-08-29)


### Bug Fixes

* small changes in the DPI ([a75925e](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/a75925ea4dec481a6922bb1245fae817c1a706a3))



## [3.9.11](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.9.10...v3.9.11) (2023-08-29)


### Bug Fixes

* erased testconfig for vite ([d05b1e9](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/d05b1e998db0cb702e004e14a53ceb550a3d73cc))
* fixed the Catalogue URL ([730cf57](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/730cf57c801b5e10dd746a6ab981c4fb8b0dad7f))



## [3.9.10](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.9.9...v3.9.10) (2023-08-25)



## [3.9.9](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.9.8...v3.9.9) (2023-08-25)


### Bug Fixes

* fixed step bar ([6f03ed7](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/6f03ed79b08307ba107a77f910923d2f27d2aad0))



## [3.9.8](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.9.7...v3.9.8) (2023-08-25)


### Bug Fixes

* reverted Stepper Styling ([589187b](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/589187bd3a2e1d6b144293dc1ab7b1d93cd4e0ce))



## [3.9.7](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.9.6...v3.9.7) (2023-08-24)



## [3.9.6](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.9.5...v3.9.6) (2023-08-24)


### Bug Fixes

* modal for invalid values for temporal resolution ([07000a5](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/07000a50455494d5e704b37c48fb3a16962a843f))
* show Unbekannt if neither modification nor release date is available ([05372c1](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/05372c11459d15f32772ae87ac53864417ce4deb))
* typo modificationDate -> releaseDate ([8706929](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/870692986116da1f7e53120a1a243a2994685355))



## [3.9.5](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.9.4...v3.9.5) (2023-08-23)


### Bug Fixes

* filter duplicate distribution content entries ([1c035f0](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/1c035f0072698de0d3f0a317f7aa76a75b994b7b))
* remove leftover distributionCanShowMore props ([eece3d1](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/eece3d17be1ff444fe9eb11c54feb30b53c6db2f))


### Features

* add distribution updateDate content ([2469910](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/2469910cfc27df9df48052f4971abbc6c5df70c3))



## [3.9.4](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.9.3...v3.9.4) (2023-08-22)


### Features

* add option to specify order and names of data to show for distribution ([bc89b89](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/bc89b8913b4399c593b37d3314992f7d8dcd9faa))
* allow passing distribution feature list as props ([8430b16](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/8430b1684f33212bc78d20ea49ea98b80e7ab430))



## [3.9.3](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.9.2...v3.9.3) (2023-08-21)


### Bug Fixes

* annif system ([046c072](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/046c072ee61bcef39f1b7e4e1750d5caa2b2351d))


### Features

* add more classes to style distributions header ([23f8dd6](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/23f8dd6d739582947a9e29c52415790c025391c6))



## [3.9.2](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.9.1...v3.9.2) (2023-08-16)


### Bug Fixes

* export DatasetDetailsFeatures to allow it to be overwritten in feature project ([cf4f021](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/cf4f021f74ae2ce74965c96648093e91a0e9b4e1))



## [3.9.1](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.9.0...v3.9.1) (2023-08-16)


### Bug Fixes

* specify style lang ([5b1bf1c](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/5b1bf1c0107075f58cfac8173cc1d105d498c7f5))


### Features

* separate distributions header title and count ([86bd3f3](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/86bd3f3918010ed7208227fb59d020e762ac04ef))



# [3.9.0](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.8.1...v3.9.0) (2023-08-15)


### Bug Fixes

* code styling ([47de71f](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/47de71f76373adf00dc46c24ff0ba6ef5ca0f2ad))
* get catalog id from domain ([86351a6](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/86351a695a654e0b55592b9dfe6dd835574f6e9a))



## [3.8.1](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.8.0...v3.8.1) (2023-08-14)


### Bug Fixes

* catalog nav skipping and overflow ([dd37821](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/dd3782152259f687c2f19e9c375828c85eb8687f))



# [3.8.0](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.7.0...v3.8.0) (2023-08-11)


### Features

* update catalog page design ([706af44](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/706af44c79682c3290144660b0679b357aeeaf6a))



# [3.7.0](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.6.20...v3.7.0) (2023-08-04)


### Features

* **SelectedFacetsOverview:** wrap pill with slot for customizing pills on view layer only ([956fc2e](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/956fc2ede8aba9018d7f82e0054b90be40bb1817))



## [3.6.20](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.6.19...v3.6.20) (2023-08-03)


### Bug Fixes

* nav dropdown hover bug ([648f59b](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/648f59bfd1277b2f18ffbd3ac257511ffed20d4b))



## [3.6.18](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.6.17...v3.6.18) (2023-08-02)



## [3.6.19](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.6.18...v3.6.19) (2023-08-02)


### Bug Fixes

* reverted dsOverview ([68e5edb](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/68e5edb3d445db8cb9847177d3e747f024db3a9d))



## [3.6.18](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.6.17...v3.6.18) (2023-08-02)



## [3.6.17](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.6.16...v3.6.17) (2023-08-02)



## [3.6.16](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.6.15...v3.6.16) (2023-08-02)


### Bug Fixes

* made the annif system configurable ([d179c70](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/d179c70768fe868c7fed4d39f0ee3654ece5a7ab))



## [3.6.15](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.6.14...v3.6.15) (2023-07-31)



## [3.6.14](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.6.13...v3.6.14) (2023-07-31)


### Bug Fixes

* catalogue page add key ([3f32e90](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/3f32e9029abb36b2bb7f0f9a93b42a33f7667ab4))



## [3.6.13](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.6.12...v3.6.13) (2023-07-28)


### Bug Fixes

* catalogue page missing items ([70bbde1](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/70bbde14f746fae6e6917496a997bf690cb088e9))



## [3.6.12](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.6.11...v3.6.12) (2023-07-28)



## [3.6.11](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.6.10...v3.6.11) (2023-07-28)


### Bug Fixes

* small fixes in the DPI ([bd8f5b2](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/bd8f5b2ce7808da1918a8af735fbf1e9503511b0))



## [3.0.22](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.6.1...v3.0.22) (2023-07-19)



## [3.6.10](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.6.9...v3.6.10) (2023-07-28)



## [3.6.9](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.6.8...v3.6.9) (2023-07-27)


### Features

* overlap background image on catalog page ([45d3529](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/45d352971e3a52c73319b83fb232c12b905236a3))



## [3.6.8](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.6.7...v3.6.8) (2023-07-26)



## [3.6.7](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.6.6...v3.6.7) (2023-07-26)



## [3.6.6](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.6.5...v3.6.6) (2023-07-21)



## [3.6.5](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.6.4...v3.6.5) (2023-07-19)


### Bug Fixes

* CatalogPage: ([eeca06c](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/eeca06cbcfb66bec66f3f81503f7f0f72f0f46fb))



## [3.6.4](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.6.3...v3.6.4) (2023-07-19)



## [3.6.3](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.6.2...v3.6.3) (2023-07-19)


### Bug Fixes

* CatalogPage cleanup ([4bf0e03](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/4bf0e0345f786a482e636f5353f369a170e9b5fe))
* CatalogPage: allow chosing custom 'Interesting datasets' in frontend ([40b0f35](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/40b0f353334f8909d13bdb20402a0aea7fab273a))
* CatalogPage: calculate background height/margin ([39f0b9e](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/39f0b9edf18e33b7193f8322aba1bc1ffd159215))



## [3.6.2](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.0.22...v3.6.2) (2023-07-19)



## [3.6.1](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.6.0...v3.6.1) (2023-07-18)


### Bug Fixes

* CatalogPage ([d56760d](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/d56760d6894a00490b002ae6d0df801a2c849955))



# [3.6.0](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.5.5...v3.6.0) (2023-07-14)


### Features

* **adapter:** append 'distributions.license,categories.label,publisher' to the includes query when searching for datasets ([9e96bbb](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/9e96bbb726769afc79d3b875faa5487221221db9))
* **Datasets:** introduce dataset filters related slots; exports DatasetFiltersTabs ([1a90543](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/1a9054346c9eab6ba284565be3ca2e3c4de7baa6))



## [3.5.5](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.5.4...v3.5.5) (2023-07-11)


### Bug Fixes

* CatalogPage: fix email addresses starting with 'mailto:' ([d2754ba](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/d2754baa9d27cc69bab9dc2aabfc80b717ad4bf9))



## [3.5.4](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.5.3...v3.5.4) (2023-07-06)


### Bug Fixes

* CatalogPage minor styling ([f2d5f88](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/f2d5f88095522c6117e21cce6945dd10827094c1))



## [3.5.3](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.5.2...v3.5.3) (2023-07-06)


### Bug Fixes

* catalogPage styling ([08e3e57](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/08e3e57012e31170bec74b5cbcacf8a1eb8b984a))



## [3.5.2](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.5.1...v3.5.2) (2023-07-06)


### Bug Fixes

* expand distribution can show more checks ([df0f9f4](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/df0f9f4e7b48e3030896e1d7a8375d2729eb1f51))
* fix console errors in CatalogPage ([fdcf398](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/fdcf398829f1807a3309547cc7ce7c00a63da8fb))



## [3.5.1](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.5.0...v3.5.1) (2023-07-06)


### Bug Fixes

* catalogPage: adjust CatalogPageDatasetCard.vue according to OP redesign ([1550dd0](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/1550dd0c43de9624063b1741b2417c99b99add86))



# [3.5.0](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.4.0...v3.5.0) (2023-07-06)


### Features

* export defineUserConfig util function from main package ([22676c1](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/22676c13a38099bc30db5a344db094b7b6da5ca6))



# [3.4.0](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.3.4...v3.4.0) (2023-07-06)


### Features

* implement config schema ([f1f6f35](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/f1f6f35d70df5a89def93d3aff5210efe1c7b1a0))



## [3.3.4](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.3.3...v3.3.4) (2023-07-05)


### Features

* generate and export type declaration files ([de153b1](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/de153b105b2cdee5da760368ddf317c5ba46b1b6))



## [3.3.3](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.3.2...v3.3.3) (2023-06-30)



## [3.3.2](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.3.1...v3.3.2) (2023-06-29)



## [3.3.1](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.3.0...v3.3.1) (2023-06-29)



# [3.3.0](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.2.1...v3.3.0) (2023-06-29)


### Features

* **DatasetList:** add raw-dataset and index slot props ([df275db](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/df275db172b6c68084f1579e3bf8201326fd12cb))



## [3.2.1](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.2.0...v3.2.1) (2023-06-29)


### Bug Fixes

* **PvDataInfoBox:** fix undefined router link target path ([cf70d14](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/cf70d14e4cb61989635bbd433f9b97891ecfdbdc))



# [3.2.0](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.1.2...v3.2.0) (2023-06-29)


### Features

* add PvDataInfoBoxFormats component ([b85f736](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/b85f736cd93359c754522ae8152d31b0ed08b138))
* **DatasetList:** export new component DatasetList ([5c7c0fa](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/5c7c0faaee7c9e43ac69ac5a312926827754aeaa))
* **Datasets:** implement Datasets slots for customizable layouts; refactor PvDataInfoBox related logic to dedicated composable ([3339fc7](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/3339fc79715f621fe93155b4c1d17021b4824c95))
* **Datasets:** implement slot for dataset search results message box ([498cbc4](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/498cbc4ddc6342ae690510eb52220aba527f16dc))
* export all PvDataInfoBox related components ([762e058](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/762e058bdd1ab91732b46f57013d2a4475a5ca9b))
* **PvDataInfoBox:** add slot for badge column ([ebb29e0](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/ebb29e086f85f18b4c0fd283c06500127151463f))
* **PvDataInfoBox:** expose dataset slot prop ([29a8a52](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/29a8a528fa12b8a40b68ccca65b677afdbf343d2))



## [3.1.2](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.1.1...v3.1.2) (2023-06-29)


### Bug Fixes

* datasets.ts ([b7e7654](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/b7e7654348ef905b9f5c485b114c209c34a48dfc))
* merge conflicts ([ac01265](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/ac01265ca1907eb643b3de7e74f00908b8cca349))
* remove Radiofacet import ([62b6a24](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/62b6a24a0e6e51368000a7ec49986e011ae8ee6d))



## [3.1.1](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.1.0...v3.1.1) (2023-06-28)



# [3.1.0](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.0.20...v3.1.0) (2023-06-28)


### Features

* allow limiting number of fetched similar datasets ([2e82713](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/2e82713895f1ea4bd1da5e28dc309751bb665836))
* Export all Vue components from modules in index.mjs ([32efbe3](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/32efbe3c62d0a2d5ab880ed0ddca4e18973624ad))



## [3.0.22](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.6.1...v3.0.22) (2023-07-19)


### Bug Fixes

* adapted RDF converter to handle temporal period ([dbed6af](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/dbed6afd19e193bb182e762258f10b37c8736ed0))
* added missing comma ([eda2afa](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/eda2afa3b37d931d6b589697dbac89a706e6ebd2))
* added note for other available languages for page properties on overview page ([d6fd925](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/d6fd925f4612abd7a79e987369826624ea96adf3))
* fixed temporal to handle time periods ([d0adcf9](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/d0adcf937e1c31cccef7b623325e2bc9d5e65af1))
* handling of page title and description on overview page ([8a3e417](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/8a3e4171b234eb12c954c623faf653425f957990))
* proper display of page title or description ([aea2f96](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/aea2f96070e0860c4fa2590c659f293b911cb1f7))
* URL Validation added // also updated the DCATAP.DE Specification ([21b4a23](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/21b4a23df40664a2ee4563e5866fc0b6bbdec2a0))



## [3.6.1](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.6.0...v3.6.1) (2023-07-18)


### Bug Fixes

* CatalogPage ([d56760d](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/d56760d6894a00490b002ae6d0df801a2c849955))



# [3.6.0](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.5.5...v3.6.0) (2023-07-14)


### Features

* **adapter:** append 'distributions.license,categories.label,publisher' to the includes query when searching for datasets ([9e96bbb](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/9e96bbb726769afc79d3b875faa5487221221db9))
* **Datasets:** introduce dataset filters related slots; exports DatasetFiltersTabs ([1a90543](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/1a9054346c9eab6ba284565be3ca2e3c4de7baa6))



## [3.5.5](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.5.4...v3.5.5) (2023-07-11)


### Bug Fixes

* CatalogPage: fix email addresses starting with 'mailto:' ([d2754ba](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/d2754baa9d27cc69bab9dc2aabfc80b717ad4bf9))



## [3.5.4](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.5.3...v3.5.4) (2023-07-06)


### Bug Fixes

* CatalogPage minor styling ([f2d5f88](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/f2d5f88095522c6117e21cce6945dd10827094c1))



## [3.5.3](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.5.2...v3.5.3) (2023-07-06)


### Bug Fixes

* catalogPage styling ([08e3e57](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/08e3e57012e31170bec74b5cbcacf8a1eb8b984a))
* expand distribution can show more checks ([df0f9f4](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/df0f9f4e7b48e3030896e1d7a8375d2729eb1f51))



## [3.5.2](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.5.1...v3.5.2) (2023-07-06)


### Bug Fixes

* fix console errors in CatalogPage ([fdcf398](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/fdcf398829f1807a3309547cc7ce7c00a63da8fb))



## [3.5.1](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.5.0...v3.5.1) (2023-07-06)


### Bug Fixes

* catalogPage: adjust CatalogPageDatasetCard.vue according to OP redesign ([1550dd0](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/1550dd0c43de9624063b1741b2417c99b99add86))



# [3.5.0](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.4.0...v3.5.0) (2023-07-06)


### Features

* export defineUserConfig util function from main package ([22676c1](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/22676c13a38099bc30db5a344db094b7b6da5ca6))



# [3.4.0](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.3.4...v3.4.0) (2023-07-06)


### Features

* generate and export type declaration files ([de153b1](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/de153b105b2cdee5da760368ddf317c5ba46b1b6))
* implement config schema ([f1f6f35](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/f1f6f35d70df5a89def93d3aff5210efe1c7b1a0))



## [3.3.4](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.3.3...v3.3.4) (2023-07-05)



## [3.3.3](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.3.2...v3.3.3) (2023-06-30)



## [3.3.2](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.3.1...v3.3.2) (2023-06-29)



## [3.3.1](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.3.0...v3.3.1) (2023-06-29)



# [3.3.0](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.2.1...v3.3.0) (2023-06-29)


### Features

* **DatasetList:** add raw-dataset and index slot props ([df275db](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/df275db172b6c68084f1579e3bf8201326fd12cb))



## [3.2.1](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.2.0...v3.2.1) (2023-06-29)


### Bug Fixes

* **PvDataInfoBox:** fix undefined router link target path ([cf70d14](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/cf70d14e4cb61989635bbd433f9b97891ecfdbdc))



# [3.2.0](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.1.2...v3.2.0) (2023-06-29)


### Features

* add PvDataInfoBoxFormats component ([b85f736](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/b85f736cd93359c754522ae8152d31b0ed08b138))
* **DatasetList:** export new component DatasetList ([5c7c0fa](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/5c7c0faaee7c9e43ac69ac5a312926827754aeaa))
* **Datasets:** implement Datasets slots for customizable layouts; refactor PvDataInfoBox related logic to dedicated composable ([3339fc7](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/3339fc79715f621fe93155b4c1d17021b4824c95))
* **Datasets:** implement slot for dataset search results message box ([498cbc4](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/498cbc4ddc6342ae690510eb52220aba527f16dc))
* export all PvDataInfoBox related components ([762e058](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/762e058bdd1ab91732b46f57013d2a4475a5ca9b))
* **PvDataInfoBox:** add slot for badge column ([ebb29e0](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/ebb29e086f85f18b4c0fd283c06500127151463f))
* **PvDataInfoBox:** expose dataset slot prop ([29a8a52](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/29a8a528fa12b8a40b68ccca65b677afdbf343d2))



## [3.1.2](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.1.1...v3.1.2) (2023-06-29)


### Bug Fixes

* datasets.ts ([b7e7654](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/b7e7654348ef905b9f5c485b114c209c34a48dfc))
* merge conflicts ([ac01265](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/ac01265ca1907eb643b3de7e74f00908b8cca349))
* remove Radiofacet import ([62b6a24](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/62b6a24a0e6e51368000a7ec49986e011ae8ee6d))


### Features

* Export all Vue components from modules in index.mjs ([32efbe3](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/32efbe3c62d0a2d5ab880ed0ddca4e18973624ad))



## [3.1.1](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.1.0...v3.1.1) (2023-06-28)



# [3.1.0](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.0.20...v3.1.0) (2023-06-28)


### Bug Fixes

* changes in the distribution stepper ([5fb3dc6](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/5fb3dc6f592b9b7d9f0b7cdb52d6cb2f90891b17))
* correction in metrics calls and dropdown links ([d0ec41e](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/d0ec41e8517b690494c368d6ae30f0ebec4fe996))
* fixed styling for temporal resolution. ([b68edb1](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/b68edb18d7fec32503ebd2ab90ae8225da367c5b))
* styling fixes ([8eb18b5](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/8eb18b55936d29d4fdb91592974688485dc17a1a))


### Features

* allow limiting number of fetched similar datasets ([2e82713](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/2e82713895f1ea4bd1da5e28dc309751bb665836))



## [3.0.20](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.0.19...v3.0.20) (2023-06-22)


### Bug Fixes

* recover CatalogPage from revert pt. 2 ([cda412d](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/cda412d5fa3ffa337b41ac7a4c1e626d415fa14f))



## [3.0.19](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.0.18...v3.0.19) (2023-06-22)


### Bug Fixes

* recover CatalogPage from revert ([e4402b6](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/e4402b61598c6bc2dbbc6923a94308ddac9d012e))



## [3.0.18](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.0.17...v3.0.18) (2023-06-22)


### Bug Fixes

* created a new custom field and fixed/restyled the User Catalogue Page ([4d88222](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/4d88222b161f8d5d3bd5a017ae1cd95c9b0cc030))



## [3.0.17](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.0.16...v3.0.17) (2023-06-21)


### Bug Fixes

* minor styling ([45c6497](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/45c6497904a7e2c80afe7ff298703e0e964cead8))



## [3.0.16](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.0.15...v3.0.16) (2023-06-21)


### Bug Fixes

* minor styling ([5a8a92b](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/5a8a92bc6c91a28db30c90b121f9b720fe7537e6))



## [3.0.15](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.0.14...v3.0.15) (2023-06-21)



## [3.0.14](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.0.13...v3.0.14) (2023-06-21)



## [3.0.13](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.0.12...v3.0.13) (2023-06-21)



## [3.0.12](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.0.11...v3.0.12) (2023-06-20)


### Bug Fixes

* fixed the mandatory field notation ([cd55a62](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/cd55a6290a7c0e20b22458cef60f3a0f4add4613))
* minor fixes in the notification modal and styling of the clear button ([2c72116](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/2c72116cf4768922ac2f1d464057415016821eef))
* minor fixes in the styling ([4a44782](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/4a4478265b6325b8164cae6e0a7538f45ebd504a))



## [3.0.11](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.0.10...v3.0.11) (2023-06-19)



## [3.0.10](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.0.9...v3.0.10) (2023-06-19)



## [3.0.9](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.0.8...v3.0.9) (2023-06-19)



## [3.0.8](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.0.6...v3.0.8) (2023-06-19)


### Bug Fixes

* issue 2896 Change label on dataset page. change the "Distribution added" label to "updated" in the Distributions table ([f23222a](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/f23222a147d80b38d5a0c07a59d60ae33cba77a9))
* removed machine translations from edit mode ([bca8fc8](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/bca8fc86442e3e46a8e266a9ac3e24689525e919))


### Features

* issue 2896 Change label on dataset page. change the "Distribution added" label to "updated" in the Distributions table ([2d58bc4](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/2d58bc403ddfab75155599ac747fd33ae78307a8))



## [3.0.6](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.0.4...v3.0.6) (2023-06-15)



## [3.0.4](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.0.3...v3.0.4) (2023-06-13)



## [3.0.3](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.0.2...v3.0.3) (2023-06-09)


### Bug Fixes

* **PvBadge:** fix syntax error ([7e9a046](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/7e9a046da21bbc73ca42e252b4c068eca712ae90))



## [3.0.2](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/compare/v3.0.1...v3.0.2) (2023-06-09)


### Bug Fixes

* **PvDataInfoBox:** fix default dates; improve vue i18n typescript integration ([3308e39](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/3308e390567981c62d9d7a44d821056dadf717ac))
* remove unreachable types field ([09349f8](https://gitlab.fokus.fraunhofer.de/piveau/ui/piveau-ui/commit/09349f839a2be4fde0c53f80978e337ea78ede4b))



